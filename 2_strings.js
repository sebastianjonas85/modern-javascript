
// Formatting 
console.log("!!! this seems not to be working on mac");

var myFormat = new Intl.NumberFormat("de-DE");
console.log(myFormat.format(1234567.89));

myFormat = new Intl.NumberFormat("de-DE", { style: "currency", currency: "EUR" })
console.log(myFormat.format(1234567.89));

myFormat = new Intl.DateTimeFormat("de-DE");
console.log(myFormat.format(new Date("2018-12-24")))


// EC6
"hello".startsWith("ello", 1) // true
"hello".endsWith("hell", 4)   // true
"hello".includes("ell")       // true
"hello".includes("ell", 1)    // true
"hello".includes("ell", 2)    // false

// classic 
"hello".indexOf("ello") === 1;    // true
"hello".indexOf("hell") === (4 - "hell".length); // true
"hello".indexOf("ell") !== -1;    // true
"hello".indexOf("ell", 1) !== -1; // true
"hello".indexOf("ell", 2) !== -1; // false
