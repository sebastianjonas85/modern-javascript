var user = { id: 42, name: "Klaus" };

// classic syntax
console.log(`Hallo ${user.name}, deine UserId ist ${user.id}`);

// EC6 syntax
console.log('Hallo ' + user.name + ', deine UserId ist ' + user.id);