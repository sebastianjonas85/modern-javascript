
//classic syntax

function someFunction(someParameter, anotherParameter) {
    console.log(someParameter, anotherParameter);
}
someFunction("some value")

var classicObject = {
    foo: function (a, b) {
        console.log(a, b);
    },
    bar: function (x, y) {
        console.log(x, y);
    }
};

classicObject.bar("a", "b");


// EC6 syntax

var someFunction = (someParameter, anotherParameterWithDefaultValue = 8) => {
    console.log(someParameter, anotherParameterWithDefaultValue);
}
someFunction("some value");

var ec6object = {
    foo: () => {
        console.log("foo");
    },
    bar: (x, y) => {
        console.log(x, y);
    }
};

ec6object.bar("a", "b");

