
function someSlowFunction(someParameter, callback) {
    setTimeout(function () {
        callback(someParameter);
    }, 5000);
}

someSlowFunction("some value", function (param) {
    console.log(param);
});

console.log("reached end of script");


//todo: make it ES6 ready