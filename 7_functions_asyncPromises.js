
function someSlowFunction() {
    return new Promise(function (resolve, reject) {
        try {
            setTimeout(function () {
                resolve("success");
            }, 5000);
        } catch (error) {
            reject(error);
        }
    });
}

someSlowFunction()
    .then(function (param) {
        console.log(param);
    })
    .catch(function (error) {
        console.log("error" + error);
    });

console.log("reached end of script");



//todo: make it ES6 ready