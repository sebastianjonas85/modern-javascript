
let someSlowFunction = async () => {
    await setTimeout(() => {
        console.log("slow function finished");
    }, 5000);
}
someSlowFunction();

console.log("reached end of script");