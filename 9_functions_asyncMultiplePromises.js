
let someSlowFunction = () => {
    return new Promise((resolve, reject) => {
        try {
            setTimeout(() => {
                resolve("slow function finished");
            }, 5000);
        } catch (error) {
            reject(error);
        }
    });
}

let slowFunctionPromise = someSlowFunction();
let intergerVariable = 1;
let stringVariable = "hallo";
let synchronousFunction = () => {
    return "synchronousFunction ready";
}

Promise.all([intergerVariable, stringVariable, synchronousFunction(), someSlowFunction()]).then((listOfPromises) => {
    console.log("all promises resolved", listOfPromises);
});

Promise.race([intergerVariable, stringVariable, synchronousFunction(), someSlowFunction()]).then((winner) => {
    console.log("and the winner is: ", winner);
});

console.log("reached end of script");